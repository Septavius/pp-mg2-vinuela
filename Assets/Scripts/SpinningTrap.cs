using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningTrap : MonoBehaviour
{
    public float radius;
    public float rotationSpeed;

    public Vector3 center;

    private float rotationAngle;


    void Start()
    {
        rotationAngle = 0f;

        if(center == Vector3.zero)
        {
            center = transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float x = Mathf.Cos(rotationAngle) * radius;
        float y = Mathf.Sin(rotationAngle) * radius;

        transform.position = new Vector3(center.x + x, center.y + y, center.z);

        rotationAngle += rotationSpeed * Time.deltaTime;

        if(rotationAngle > 2 * Mathf.PI)
        {
            rotationAngle -= 2 * Mathf.PI;
        }
    }
}
