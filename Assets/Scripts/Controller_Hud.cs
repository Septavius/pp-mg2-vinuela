﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public Text gameOverText;
    public Text tutorialText;
    void Start()
    {
        gameOverText.gameObject.SetActive(false);
    }

    void Update()
    {
        if (GameManager.gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over" ;
            gameOverText.gameObject.SetActive(true);
        }
        if (GameManager.winCondition)
        {
            Time.timeScale = 0;
            gameOverText.text = "You Win";
            gameOverText.gameObject.SetActive(true);
        }

        aboutBlocks();
    }

    private void aboutBlocks()
    {
        if(GameManager.actualPlayer == 0)
        {
            tutorialText.text = "Common block";
        }
        else if(GameManager.actualPlayer == 1)
        {
            tutorialText.text = "Small block";
        }
        else if(GameManager.actualPlayer == 2)
        {
            tutorialText.text = "Tall block: use it to help you climb!";
        }
        else if (GameManager.actualPlayer == 3)
        {
            tutorialText.text = "Floating block: use it to go through the water!";
        }
        else if (GameManager.actualPlayer == 4)
        {
            tutorialText.text = "Trampoline block: jump over it to reach high places!";
        }
        else if (GameManager.actualPlayer == 5)
        {
            tutorialText.text = "Inverted block: walking on ceilings? Easy.";
        }
        else if (GameManager.actualPlayer == 6)
        {
            tutorialText.text = "Double jump block: fits anywhere, goes anywhere.";
        }
        else if (GameManager.actualPlayer == 7)
        {
            tutorialText.text = "Resize block: your skyscraper friend.";
        }
    }
}
