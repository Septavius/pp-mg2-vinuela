using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Resize : Controller_Player
{
    float resizeFactor = 3f;

    private Vector3 originalScale;
    private Vector3 resizePosition;
    bool resized;

    public override void Start()
    {
        originalScale = transform.localScale;
        resized = false;
        Debug.Log(resized);
        base.Start();
    }

    public override void Update()
    {
        if (GameManager.actualPlayer == playerNumber)
        {
            checkInput();
        }
        base.Update();
    }

    private void checkInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(resized == false)
            {
                resizePosition = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
                transform.position = resizePosition; //para evitar noclips en el piso
                Vector3 newSize = new Vector3(originalScale.x, originalScale.y * resizeFactor, originalScale.z);
                transform.localScale = newSize;
                resized = true;
            }

            else if(resized == true)
            {
                transform.localScale = originalScale;
                resized = false;
            }
        }
    }
}
