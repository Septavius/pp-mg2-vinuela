using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt_Projectile : Projectile
{
    // Start is called before the first frame update
    public void Initialize(Vector3 fireDirection)
    {
        this.rb = GetComponent<Rigidbody>();
        base.Movement(fireDirection);
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Floor") || other.CompareTag("Button") || other.CompareTag("Door"))
        {
            Destroy(this.gameObject);
        }
    }
}
