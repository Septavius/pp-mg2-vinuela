using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : Pickup
{
    public enum KeyColor
    {
        None,
        Red,
        Blue,
        White,
        Green
    }

    public KeyColor keyColor;

    private static Dictionary<KeyColor, bool> keyPickedUp = new Dictionary<KeyColor, bool>
    {
        { KeyColor.Red, false },
        { KeyColor.Blue, false },
        { KeyColor.Green, false },
        { KeyColor.White, false }
    }; //DICCIONARIO DE COLORES Y ESTADOS. PERMITEN LEER EN PARES MANDANDO COMO PARAMETRO EL COLOR A CONSULTAR / ALTERAR

    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            if(keyPickedUp[keyColor] == false) //si la llave del color que toque aun no fue agarrada:
            {
                keyPickedUp[keyColor] = true;
                Destroy(this.gameObject);
                Debug.Log("Agarraste la llave: " + keyColor);
            }
        }
    }

    public static bool pickedUp(KeyColor color)
    {
        return keyPickedUp[color]; //me devuelve si la llave est� agarrada o no
    }

    public static void ResetKeys()
    {
        List<KeyColor> keys = new List<KeyColor>(keyPickedUp.Keys);
        foreach (KeyColor key in keys)
        {
            keyPickedUp[key] = false;
        }
    }
}
