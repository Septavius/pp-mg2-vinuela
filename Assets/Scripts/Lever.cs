using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    private Animator anim;

    public static bool leverOn;
    private void Start()
    {
        anim = this.GetComponentInParent<Animator>();
        anim.SetBool("leverOn", false);
        leverOn = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            anim.SetBool("leverOn", true);
            leverOn = true;
        }
    }
}
