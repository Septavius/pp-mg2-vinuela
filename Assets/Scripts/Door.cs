using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Key.KeyColor doorColor;

    private static Dictionary<Key.KeyColor, bool> doorOpened = new Dictionary<Key.KeyColor, bool>
    {
        { Key.KeyColor.Red, false },
        { Key.KeyColor.Blue, false },
        { Key.KeyColor.Green, false },
        { Key.KeyColor.White, false }
    };

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player") && Key.pickedUp(doorColor))
        {
            Debug.Log(doorColor + " door opened.");
            doorOpened[doorColor] = true;
            Destroy(this.gameObject);
        }
    }

    public static bool isDoorOpen(Key.KeyColor color)
    {
        return doorOpened[color]; //me devuelve si la llave est� agarrada o no
    }

    public static void doorReset()
    {
        List<Key.KeyColor> doors = new List<Key.KeyColor>(doorOpened.Keys);
        foreach (Key.KeyColor door in doors)
        {
            doorOpened[door] = false;
        }
    }
}
