using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Projectile : MonoBehaviour
{
    [SerializeField] float projectileSpeed;
    [SerializeField] protected Rigidbody rb;

    public virtual void Movement(Vector3 direction)
    {
        rb.velocity = direction * projectileSpeed * Time.deltaTime;
    }

    public abstract void OnTriggerEnter(Collider other);
}
