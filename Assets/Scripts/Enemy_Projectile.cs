using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Projectile : Projectile
{
    [SerializeField] private Turret turret;

    public void Initialize(Turret turret, Vector3 fireDirection)
    {
        this.turret = turret;
        this.rb = GetComponent<Rigidbody>();
        base.Movement(fireDirection);
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Floor") || other.CompareTag("Player"))
        {
            turret.ProjectileDestroyed();
            Destroy(this.gameObject);
        }
    }
}

