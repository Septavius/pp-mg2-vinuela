using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    public bool goDown;
    private Rigidbody rb;
    public float elevatorForce = 125f;
    // Start is called before the first frame update
    void Start()
    {
        goDown = true;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Lever.leverOn == true)
        {
            if(goDown == true)
            {
                rb.velocity = Vector3.down * elevatorForce * Time.deltaTime;
            }
            else if (goDown == false)
            {
                rb.velocity = Vector3.up * elevatorForce * Time.deltaTime;
            }
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }

    private void Update()
    {
        if (transform.position.y >= 18)
        {
            goDown = true;
        }
        if (transform.position.y <= 5)
        {
            goDown = false;
        }
    }
}
