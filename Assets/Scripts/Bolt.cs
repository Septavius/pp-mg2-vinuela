using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : Pickup
{
    public override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Controller_Player.fireActive = true;
            Destroy(this.gameObject);
            Debug.Log(Controller_Player.fireActive);
        }
    }
}
