using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    private float rotationSpeed = 100f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pickupRotation();
    }

    private void pickupRotation()
    {
        transform.Rotate(0, rotationSpeed * Time.deltaTime, rotationSpeed * Time.deltaTime, Space.World);
    }

    public abstract void OnTriggerEnter(Collider other);
}
