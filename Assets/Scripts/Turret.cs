using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public enum Zone //zonas trigger
    {
        A,
        B
    }

    public Zone turretZone;

    public enum fireDirection //Permite la universalidad a la hora de configurar la torreta
    {
        Up,
        Down,
        Left,
        Right
    }

    public fireDirection direction;
    private Vector3 firingDirection;

    private bool canFire;

    public GameObject projectile;
    private GameObject currentProjectile;
    // Start is called before the first frame update
    void Start()
    {
        canFire = true;

        if(direction == fireDirection.Down)
        {
            firingDirection = Vector3.down;
        }
        else if(direction == fireDirection.Up)
        {
            firingDirection = Vector3.up;
        }
        else if (direction == fireDirection.Left)
        {
            firingDirection = Vector3.left;
        }
        else
        {
            firingDirection = Vector3.right;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if(turretZone == Zone.A)
        {
            if (Door.isDoorOpen(Key.KeyColor.White))
            {
                if(canFire && currentProjectile == null)
                {
                    FireProjectile();
                }
            }
        }
    }

    public void ProjectileDestroyed()
    {
        canFire = true;
    }

    private void FireProjectile()
    {
        currentProjectile = Instantiate(projectile, transform.position, Quaternion.identity);
        Enemy_Projectile prScript = currentProjectile.GetComponent<Enemy_Projectile>();
        if(prScript != null)
        {
            prScript.Initialize(this,firingDirection); //inicializo los proyectiles asignandoles a cada uno su respectiva torreta (firepoint)
            canFire = false;
        }
    }

}
